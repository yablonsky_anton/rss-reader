package ru.rit_it.rssreaderapp.activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import ru.rit_it.rssreaderapp.Item.Item;
import ru.rit_it.rssreaderapp.R;
import ru.rit_it.rssreaderapp.adapter.ItemsAdapter;

//activity c лентой
public class FeedActivity extends AppCompatActivity {

    private static final String TAG = "FeedActivity";
    private RecyclerView recyclerView;
    private List<Item> itemsList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        new ProcessFeed().execute((Void) null);

    }

    //Парсинг ленты
    public List<Item> parseFeed(InputStream inputStream) throws XmlPullParserException {
        Item item = null;
        List<Item> items = new ArrayList<>();
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(inputStream, null);
            parser.nextTag();
            boolean insideItem = false;
            int eventType = parser.getEventType();
            //Начало парсинга
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    if (parser.getName().equalsIgnoreCase("item")) {
                        insideItem = true;
                        item = new Item();
                    } else if (insideItem) {
                        if (parser.getName().equalsIgnoreCase("title")) {
                            item.setTitle(parser.nextText());
                        } else if (parser.getName().equalsIgnoreCase("description")) {
                            item.setDescription(parser.nextText());
                        } else if (parser.getName().equalsIgnoreCase("pubdate")) {
                            item.setPubDate(parser.nextText());
                        } else if (parser.getName().equalsIgnoreCase("link")) {
                            item.setLink(parser.nextText());
                        }
                    }
                } else if (eventType == XmlPullParser.END_TAG && parser.getName().equalsIgnoreCase("item")) {
                    insideItem = false;
                    items.add(item);
                }
                eventType = parser.next();
            }
            return items;
        } catch (IOException e) {
            Log.e(TAG, "Ошибка", e);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                Log.e(TAG, "Ошибка", e);
            }
        }
        return items;
    }

    //Класс для фоновой загрузки и обработки rss ленты
    private class ProcessFeed extends AsyncTask<Void, Void, Boolean> {

        ProgressDialog progDialog = new ProgressDialog(FeedActivity.this);
        private String urlLink;

        @Override
        protected void onPreExecute() {
            //Использование адреса переданного из MainActivity
            urlLink = getIntent().getStringExtra("link");
            //Индикция загрузки
            progDialog.setMessage("Загрузка...");
            progDialog.setIndeterminate(false);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setCancelable(true);
            progDialog.show();

        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            //Проверка введенного адреса (пустой или нет)
            if (TextUtils.isEmpty(urlLink))
                return false;

            try {
                //Если адрес введен без протоколов, то дополнить его http://
                if (!urlLink.startsWith("http://") && !urlLink.startsWith("https://")) {
                    urlLink = "http://" + urlLink;
                }

                URL url = new URL(urlLink);
                InputStream inputStream = url.openStream();

                try {
                    itemsList = parseFeed(inputStream);
                } catch (XmlPullParserException e) {
                    Log.e(TAG, "Ошибка", e);
                    //Если произошла ошибка при парсинге, то поменять протокол и попробовать
                    //снова
                    if (urlLink.startsWith("http:")) {
                        urlLink = urlLink.replaceFirst("http", "https");
                    } else if (urlLink.startsWith("https:")) {
                        urlLink = urlLink.replaceFirst("https", "http");
                    }
                    url = new URL(urlLink);
                    inputStream = url.openStream();
                    try {
                        itemsList = parseFeed(inputStream);
                    } catch (XmlPullParserException e1) {
                        Log.e(TAG, "Ошибка", e1);
                        return false;
                    }
                }
                return true;
            } catch (IOException e) {
                Log.e(TAG, "Ошибка", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success) {
                //Если в итоге парсинг прошел успешно, то передать itemsList адаптеру RecycleView
                //и убрать индикацию загрузки
                recyclerView.setAdapter(new ItemsAdapter(itemsList));
                progDialog.dismiss();
            } else {
                //Если в итоге парсинг прошел неудачно, то убрать индикацию загрузки, вывести
                //сообщение с ошибкой и завершить acitivity
                progDialog.dismiss();
                Toast.makeText(FeedActivity.this,
                        "Неправильно введен адрес или ошибка сети",
                        Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

}
