package ru.rit_it.rssreaderapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import ru.rit_it.rssreaderapp.R;

//Основное activity, которое отображается при открытии приложения
public class MainActivity extends AppCompatActivity {

    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        editText = findViewById(R.id.rss_link);

        //Мониторинг нажатия кнопки Enter на клавиатуре. После нажатия открыть activity с лентой
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    openFeedActivity(view);
                    return true;
                }
                return false;
            }
        });
    }

    //Открыть activity с лентой
    public void openFeedActivity(View view) {
        String link = editText.getText().toString();
        Intent intent = new Intent(this, FeedActivity.class);
        //Передача адреса введенного в поле основного activity новому activity ленты
        intent.putExtra("link", link);
        startActivity(intent);
    }

    //Очистка поля с введенным адресом rss ленты
    public void clearEditText(View view) {
        editText.getText().clear();
    }
}
