package ru.rit_it.rssreaderapp.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.rit_it.rssreaderapp.Item.Item;
import ru.rit_it.rssreaderapp.R;

//Адаптер для RecycleView
public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ItemViewHolder> {


    private List<Item> itemsList;

    public ItemsAdapter(List<Item> items) {
        itemsList = items;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.feed_item, viewGroup, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder viewHolder, int i) {
        final Item item = itemsList.get(i);
        ((TextView) viewHolder.itemView.findViewById(R.id.titleText)).setText(item.getTitle());
        ((TextView) viewHolder.itemView.findViewById(R.id.descriptionText)).setText(item.getDescription());
        ((TextView) viewHolder.itemView.findViewById(R.id.linkText)).setText(item.getLink());
        ((TextView) viewHolder.itemView.findViewById(R.id.pubdateText)).setText(item.getPubDate());
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }


    static class ItemViewHolder extends RecyclerView.ViewHolder {
        private View itemView;

        ItemViewHolder(View view) {
            super(view);
            itemView = view;
        }
    }
}
